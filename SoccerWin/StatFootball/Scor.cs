using System;
using System.Linq;

namespace SoccerWin.StatFootball
{
	class Scor
	{
		public int Goals1{ get; private set; }

		public int Goals2{ get; private set; }

		public double Perc{ get; private set; }

		public Scor (int g1, int g2, double x1, double x2, int max=2)
		{
			Goals1 = g1;
			Goals2 = g2;
			Perc = Foo (x1, g1, max) * Foo (x2, g2, max);
		}

		/// <summary>
		/// Foo the specified x, n and max.
		/// </summary>
		/// <param name="x">коэф a5,a6,d5 etc</param>
		/// <param name="n">голы</param>
		/// <param name="max">макс голов</param>
		private double Foo (double x, int n, int max=2)
		{

			var a = new double[max+1];
			var b = new double[max+1];
			for (var i=0; i<max+1; i++) {
				b [i] = i == 0 ? 1 : b [i - 1] * i;
				a [i] = (Math.Exp (-x) * Math.Pow (x, i)) / b [i];
			}

			if (n > max) {
				return 1 - a.Sum ();
			}
			return a [n];
		}
	}

}

