using System;

namespace SoccerWin.StatFootball
{
	class Row
	{
		public string Team{ get; set; }
		public Stat Total{ get; set; }
		public Stat Home{ get; set; }
		public Stat Away{ get; set; }
	}
}

