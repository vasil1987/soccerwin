using System;
using System.Collections.Generic;
using HtmlAgilityPack;
using System.Dynamic;
using System.Text;
using System.Linq;
using System.Configuration;

namespace SoccerWin.StatFootball
{
	class Method:IMethod
	{
		public IEnumerable<Game> GetGames ()
		{
			var doc = Load (@"http://stat-football.com/en/");
			var rows = doc.DocumentNode.SelectNodes ("//li[@class='s18']/a");

			var games = new List<Game> ();
			foreach (var row in rows) {
				var teams = row.SelectSingleNode ("text()[2]").InnerText.
					Replace ("&nbsp;", "").
					Replace ("&#8212;", "|").Split ('|');

				dynamic tag = new ExpandoObject ();
				tag.url = row.Attributes ["href"].Value;
				var game = new Game {
					Date=row.SelectSingleNode ("b").InnerText,
					Tag = tag,
					Team1 = teams[0].Trim(),
					Team2 = teams[1].Trim()
				};

				games.Add (game);
			}

			return games;
		}

		public dynamic Calc (Game game)
		{
            var doc1 = Load("http://stat-football.com" + (string)game.Tag.url);

            var foo1 = doc1.DocumentNode.SelectSingleNode("//a[text()='halftime table']");
			var foo2 = foo1.Attributes ["href"].Value;

            var doc = Load("http://stat-football.com" + foo2);

			var half1 = GetHalf (doc.DocumentNode.SelectSingleNode ("//table[@id='tb02']"));
			var half2 = GetHalf (doc.DocumentNode.SelectSingleNode ("//table[@id='tb03']"));

			var t1h1 = half1.FirstOrDefault (t => t.Team == game.Team1);
			var t2h1 = half1.FirstOrDefault (t => t.Team == game.Team2);

			var t1h2 = half2.FirstOrDefault (t => t.Team == game.Team1);
			var t2h2 = half2.FirstOrDefault (t => t.Team == game.Team2);

			//расчет вероятностей разных комбинации в первом тайме
			var half1Scores = new List<Scor> ();
			var xt1h1 = (t1h1.Home.Goals1 / t1h1.Home.Games + t2h1.Away.Goals2 / t2h1.Away.Games) / 2.0;
			var xt2h1 = (t1h1.Home.Goals2 / t1h1.Home.Games + t2h1.Away.Goals1 / t2h1.Away.Games) / 2.0;
			for (var i=0; i<=2; i++) {
				for (var j=0; j<=2; j++) {
					half1Scores.Add (new Scor (i, j, xt1h1,xt2h1 , 2));
				}
			}

			//расчет вероятностей разных комбинации во втором тайме
			var half2Scores = new List<Scor> ();
			var xt1h2 = (t1h2.Home.Goals1 / t1h2.Home.Games + t2h2.Away.Goals2 / t2h2.Away.Games) / 2.0;
			var xt2h2 = (t1h2.Home.Goals2 / t1h2.Home.Games + t2h2.Away.Goals1 / t2h2.Away.Games) / 2.0;
			for (var i=0; i<=2; i++) {
				for (var j=0; j<=2; j++) {
					half2Scores.Add (new Scor (i, j,xt1h2 ,xt2h2, 2));
				}
			}


			//расчет вероятностей разных комбинации итого
			var allScores = new List<Scor> ();
			var xt1all = xt1h1 + xt1h2;
			var xt2all = xt2h1 + xt2h2;
			for (var i=0; i<=4; i++) {
				for (var j=0; j<=4; j++) {
					allScores.Add (new Scor (i, j,xt1all ,xt2all, 4));
				}
			}



			var result = new Result ();

			var allrest = (1.0 - allScores.Sum (s=>s.Perc));
			result.SingleAll = new ResPart {
				Team1 = allScores.Where(s=>s.Goals1>s.Goals2).Sum(s=>s.Perc)+allrest/3.0,
				Team2 = allScores.Where(s=>s.Goals1<s.Goals2).Sum(s=>s.Perc)+allrest/3.0,
				TeamX = allScores.Where(s=>s.Goals1==s.Goals2).Sum(s=>s.Perc)+allrest/3.0
			};

			var h1rest = (1.0 - half1Scores.Sum (s=>s.Perc));
			result.SingleHalf1 = new ResPart {
				Team1 = half1Scores.Where(s=>s.Goals1>s.Goals2).Sum(s=>s.Perc)+h1rest/3.0,
				Team2 = half1Scores.Where(s=>s.Goals1<s.Goals2).Sum(s=>s.Perc)+h1rest/3.0,
				TeamX = half1Scores.Where(s=>s.Goals1==s.Goals2).Sum(s=>s.Perc)+h1rest/3.0
			};

			var h2rest = (1.0 - half2Scores.Sum (s=>s.Perc));
			result.SingleHalf2 = new ResPart {
				Team1 = half1Scores.Where(s=>s.Goals1>s.Goals2).Sum(s=>s.Perc)+h2rest/3.0,
				Team2 = half1Scores.Where(s=>s.Goals1<s.Goals2).Sum(s=>s.Perc)+h2rest/3.0,
				TeamX = half1Scores.Where(s=>s.Goals1==s.Goals2).Sum(s=>s.Perc)+h2rest/3.0
			};

			result.DoubleAll = new ResPart {
				Team1 = allScores.Where(s=>s.Goals1>=s.Goals2).Sum(s=>s.Perc)+allrest*2.0/3.0,
				Team2 = allScores.Where(s=>s.Goals1<=s.Goals2).Sum(s=>s.Perc)+allrest*2.0/3.0,
				TeamX = allScores.Where(s=>s.Goals1!=s.Goals2).Sum(s=>s.Perc)+allrest*2.0/3.0
			};

			result.DoubleHalf1 = new ResPart {
				Team1 = half1Scores.Where(s=>s.Goals1>=s.Goals2).Sum(s=>s.Perc)+allrest*2.0/3.0,
				Team2 = half1Scores.Where(s=>s.Goals1<=s.Goals2).Sum(s=>s.Perc)+allrest*2.0/3.0,
				TeamX = half1Scores.Where(s=>s.Goals1!=s.Goals2).Sum(s=>s.Perc)+allrest*2.0/3.0
			};

			result.DoubleHalf2 = new ResPart {
				Team1 = half2Scores.Where(s=>s.Goals1>=s.Goals2).Sum(s=>s.Perc)+allrest*2.0/3.0,
				Team2 = half2Scores.Where(s=>s.Goals1<=s.Goals2).Sum(s=>s.Perc)+allrest*2.0/3.0,
				TeamX = half2Scores.Where(s=>s.Goals1!=s.Goals2).Sum(s=>s.Perc)+allrest*2.0/3.0
			};

			result.ScoreAllOther = new Scor (5, 5, xt1h1 + xt1h2, xt2h1 + xt2h2, 4).Perc;
			result.ScoreHalf1Other = new Scor (3, 3, xt1h1, xt2h1, 2).Perc;
			result.ScoreHalf2Other = new Scor (3, 3, xt1h2, xt2h2, 2).Perc;

			result.ScoreAll = allScores.Where (s => s.Goals1 <= 2 && s.Goals2 <= 2).
				Select (s=>new ResScore{
					G1 =s.Goals1,
					G2 = s.Goals2,
					Perc = s.Perc
				}).ToArray();

			result.ScoreHalf1 = half1Scores.Where (s => s.Goals1 <= 2 && s.Goals2 <= 2).
				Select (s=>new ResScore{
					G1 =s.Goals1,
					G2 = s.Goals2,
					Perc = s.Perc
				}).ToArray();

			result.ScoreHalf2 = half2Scores.Where (s => s.Goals1 <= 2 && s.Goals2 <= 2).
				Select (s=>new ResScore{
					G1 =s.Goals1,
					G2 = s.Goals2,
					Perc = s.Perc
				}).ToArray();

			result.StrResult = "";
			if ((result.SingleAll.Team1 - result.SingleAll.Team2) > 0.19) {
				result.StrResult = "1";
			}else if(result.SingleAll.Team1 > result.SingleAll.Team2){
				result.StrResult = "1X";
			}else if ((result.SingleAll.Team2 - result.SingleAll.Team1) > 0.19) {
				result.StrResult = "2";
			}else if(result.SingleAll.Team2 > result.SingleAll.Team1){
				result.StrResult = "X";
			}

			result.Team1 = game.Team1;
			result.Team2 = game.Team2;

			result.Ovun=new OVUN[6];
			result.Ovun2=new OVUN2[6];
			for (var i=0; i<=5; i++) {
				var un = result.ScoreAll.Where (s => s.G1+s.G2 <= i).Sum (s => s.Perc);
				if (i == 5) {
					un += 1.0 - result.ScoreAll.Sum (s => s.Perc);
				}
				var ov = 1.0 - un;

				if (i > 0) {
					result.Ovun2 [i-1] = new OVUN2 {
						I = i,
						Ov = ov,
						Un = un
					};
				}

				if (un > ov) {
					result.Ovun [i] = new OVUN {
						Name = "UN " + i + ",5",
						Perc = un
					};

				} else {
					result.Ovun [i] = new OVUN {
						Name = "OV " + i + ",5",
						Perc = ov
					};
				}
			}

			result.TotalGoalsAll=new double[8];
			for (var i=0; i<8; i++) {

				if (i == 7) {
					result.TotalGoalsAll [i] = 1.0 - result.TotalGoalsAll.Sum ();
				} else if (i == 5 || i == 6) {
					result.TotalGoalsAll [i] = allScores.Where (s => s.Goals1 + s.Goals2 == i).Sum (s => s.Perc) + (1.0 - allScores.Sum (s => s.Perc))/3.0;
				}else{
					result.TotalGoalsAll[i]=allScores.Where (s => s.Goals1 + s.Goals2 == i).Sum (s => s.Perc);
				}
			}

			result.TotalGoalsHalf1=new double[4];
			for (var i=0; i<4; i++) {

				if (i == 3) {
					result.TotalGoalsHalf1 [i] =half1Scores.Where (s => s.Goals1 + s.Goals2 > i).Sum(s=>s.Perc)+ 1.0 - result.TotalGoalsHalf1.Sum ();
				} else{
					result.TotalGoalsHalf1[i]=half1Scores.Where (s => s.Goals1 + s.Goals2 == i).Sum (s => s.Perc);
				}
			}


			result.TotalGoalsHalf2=new double[4];
			for (var i=0; i<4; i++) {

				if (i == 3) {
					result.TotalGoalsHalf2 [i] =half2Scores.Where (s => s.Goals1 + s.Goals2 > i).Sum(s=>s.Perc)+ 1.0 - result.TotalGoalsHalf2.Sum ();
				} else{
					result.TotalGoalsHalf2[i]=half2Scores.Where (s => s.Goals1 + s.Goals2 == i).Sum (s => s.Perc);
				}
			}

			result.WhoWins = "";
			if (result.SingleAll.Team1 > result.SingleAll.Team2) {
				if (result.SingleAll.Team1 > result.SingleAll.TeamX) {
					result.WhoWins = "1";
				} else {
					result.WhoWins = "X";
				}
			} else {
				if (result.SingleAll.Team2 > result.SingleAll.TeamX) {
					result.WhoWins = "2";
				} else {
					result.WhoWins = "X";
				}
			}

			return result;
		}

	

		private Row[] GetHalf (HtmlNode node)
		{
			var rows = new List<Row> ();

			var htmlRows = node.SelectNodes ("tbody/tr");

			foreach (var htmlRow in htmlRows) {
				var row = new Row {
					Team = htmlRow.SelectSingleNode("td[2]").InnerText.Trim(),
					Total = new Stat{
						Games = int.Parse(htmlRow.SelectSingleNode("td[3]").InnerText),
						Wins = int.Parse(htmlRow.SelectSingleNode("td[4]").InnerText),
						Draw = int.Parse(htmlRow.SelectSingleNode("td[5]").InnerText),
						Losts = int.Parse(htmlRow.SelectSingleNode("td[6]").InnerText),
						Goals1 = int.Parse(htmlRow.SelectSingleNode("td[7]").InnerText),
						Goals2 = int.Parse(htmlRow.SelectSingleNode("td[9]").InnerText),
						Points = int.Parse(htmlRow.SelectSingleNode("td[10]").InnerText)
					},
					Home = new Stat{
						Games = int.Parse(htmlRow.SelectSingleNode("td[11]").InnerText),
						Wins = int.Parse(htmlRow.SelectSingleNode("td[12]").InnerText),
						Draw = int.Parse(htmlRow.SelectSingleNode("td[13]").InnerText),
						Losts = int.Parse(htmlRow.SelectSingleNode("td[14]").InnerText),
						Goals1 = int.Parse(htmlRow.SelectSingleNode("td[15]").InnerText),
						Goals2 = int.Parse(htmlRow.SelectSingleNode("td[17]").InnerText),
						Points = int.Parse(htmlRow.SelectSingleNode("td[18]").InnerText)
					},
					Away = new Stat{
						Games = int.Parse(htmlRow.SelectSingleNode("td[19]").InnerText),
						Wins = int.Parse(htmlRow.SelectSingleNode("td[20]").InnerText),
						Draw = int.Parse(htmlRow.SelectSingleNode("td[21]").InnerText),
						Losts = int.Parse(htmlRow.SelectSingleNode("td[22]").InnerText),
						Goals1 = int.Parse(htmlRow.SelectSingleNode("td[23]").InnerText),
						Goals2 = int.Parse(htmlRow.SelectSingleNode("td[25]").InnerText),
						Points = int.Parse(htmlRow.SelectSingleNode("td[26]").InnerText)
					}
				};
				rows.Add (row);
			}

			return rows.ToArray ();
		}

		private HtmlDocument Load (string url)
		{
			var web = new HtmlWeb ();
			web.OverrideEncoding = Encoding.GetEncoding (1251);


			if (bool.Parse(ConfigurationManager.AppSettings ["use-proxy"])) {
				return web.Load(url, ConfigurationManager.AppSettings ["proxy-url"], 
				                int.Parse(ConfigurationManager.AppSettings ["proxy-port"]), 
				                ConfigurationManager.AppSettings ["proxy-login"], ConfigurationManager.AppSettings ["proxy-pass"]);
			}
			return web.Load (url);
			//return web.Load(url, "10.4.102.19", 3228, @"MRG002\f0020531", "r123456R");
		}

		public string Id {
			get {
				return "statfootball";
			}
		}

		public string Name {
			get {
				return "HT+";
			}
		}

		public string Template {
			get {
				return "statfootball.html";
			}
		}
	}
}

