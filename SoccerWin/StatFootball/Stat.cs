using System;

namespace SoccerWin.StatFootball
{
	class Stat
	{
		public double Games{ get; set; }
		public double Wins{ get; set; }
		public double Draw{ get; set; }
		public double Losts{ get; set; }
		public double Goals1{ get; set; }
		public double Goals2{ get; set; }
		public double Points{ get; set; }
	}
}

