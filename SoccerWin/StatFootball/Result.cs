using System;

namespace SoccerWin.StatFootball
{
	class Result
	{
		public ResPart SingleAll{ get; set; }
		public ResPart SingleHalf1{ get; set; }
		public ResPart SingleHalf2{ get; set; }

		public ResPart DoubleAll{ get; set; }
		public ResPart DoubleHalf1{ get; set; }
		public ResPart DoubleHalf2{ get; set; }

		public ResScore[] ScoreAll{ get; set; }
		public ResScore[] ScoreHalf1{ get; set; }
		public ResScore[] ScoreHalf2{ get; set; }

		public double ScoreAllOther{ get; set; }
		public double ScoreHalf1Other{ get; set; }
		public double ScoreHalf2Other{ get; set; }

		public string StrResult{get;set;}

		public string Team1{ get; set; }
		public string Team2{ get; set; }

		public OVUN[] Ovun{ get; set; }
		public OVUN2[] Ovun2{ get; set; }

		public double[] TotalGoalsAll{ get; set; }
		public double[] TotalGoalsHalf1{ get; set; }
		public double[] TotalGoalsHalf2{ get; set; }

		public string WhoWins { get; set;}
	}

	class OVUN{
		public string Name{ get; set; }
		public double Perc{ get; set; }
	}

	class OVUN2{
		public int I{ get; set; }
		public double Ov{ get; set; }
		public double Un{ get; set; }
	}

	class ResPart{
		public double Team1{ get; set; }
		public double Team2{ get; set;}
		public double TeamX{ get; set; }

		public double Team1d {
			get {
				return 1.0 / Team1;
			}
		}

		public double Team2d {
			get {
				return 1.0 / Team2;
			}
		}

		public double TeamXd {
			get {
				return 1.0 / TeamX;
			}
		}
	}

	class ResScore{
		public int G1{ get; set; }
		public int G2{ get; set; }
		public double Perc{ get; set; }
	}
}

