﻿using System;
using Nancy;
using Nancy.Conventions;
using Nancy.Bootstrappers.Unity;
using Microsoft.Practices.Unity;

namespace SoccerWin
{
	public class Bootstrapper:UnityNancyBootstrapper
	{
		private IUnityContainer container;

		public Bootstrapper(IUnityContainer container){
			this.container = container;
		}

		protected override void ConfigureConventions (Nancy.Conventions.NancyConventions nancyConventions)
		{
			base.ConfigureConventions (nancyConventions);

			nancyConventions.StaticContentsConventions.Add (
				StaticContentConventionBuilder.AddDirectory("content",@"Content")
			);
		}

		protected override IUnityContainer GetApplicationContainer ()
		{
			return container;
		}
	}
}

