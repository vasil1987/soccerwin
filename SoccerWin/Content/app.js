﻿angular.module("soccerwin",["ui.bootstrap"])
.controller("mainCtrl",function($scope,$http,$log){
	
	$scope.methods=[];	
	$scope.choosenMethod=undefined;
	$scope.games=[];	
	$scope.choosenGame=undefined;
	$scope.result=undefined;
	$scope.waitForGames=false;
	$scope.waitForResult=false;
	
	$scope.chooseMethod=function(method){
	    $scope.choosenMethod = method;	    
	    $scope.refreshGames();
	};
	
	function getMethods(){
		$http.get("/methods").then(function(response){
			$scope.methods=response.data;
			$scope.choosenMethod=$scope.methods[0];
		});
	};
	
	function load(skip){				
		$http.get("/games/"+$scope.choosenMethod.id+"/"+skip).then(function(response){
			$log.debug("получена порция игр %s",response.data.length);
			for(var i=0;i<response.data.length;i++){
				$scope.games.push(response.data[i]);			
			}
			if(response.data.length>0){
				load($scope.games.length);
			}else{
				$scope.waitForGames=false;
			}
		});
	}
	
	$scope.refreshGames=function(skip){
		if(skip===undefined)skip=0;
		$scope.waitForGames=true;
		$scope.games.length=0;
		
		load(skip);
		
		/*$http.get("/games/"+$scope.choosenMethod.id+"/"+skip).then(function(response){
			$scope.games=response.data;			
			$scope.waitForGames=false;
		});*/
	};

	$scope.chooseGame=function(game){
		$scope.choosenGame=game;
		$scope.waitForResult=true;	
		$http.post("/calc/"+$scope.choosenMethod.id,game).then(function(response){
			$scope.result=response.data;
			$scope.waitForResult=false;
		});
	};
	
	$scope.searchGame="";
	$scope.gameFilter = function (game) { 
	    return $scope.searchGame==="" || game.team1.toLowerCase().indexOf($scope.searchGame.toLowerCase()) >=0 || game.team2.toLowerCase().indexOf($scope.searchGame.toLowerCase()) >=0; 
	};

	getMethods();
})
.filter('percentage', ['$filter', function ($filter) {
  return function (input, decimals) {
    return $filter('number')(input * 100, decimals) + '%';
  };
}]);