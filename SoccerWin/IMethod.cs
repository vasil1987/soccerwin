using System;
using System.Collections.Generic;

namespace SoccerWin
{
	/// <summary>
	/// метод расчета вероятности
	/// </summary>
	public interface IMethod
	{
		string Id{ get;}
		string Name{get;}
		IEnumerable<Game> GetGames();
		dynamic Calc (Game game);
		string Template{ get; }
	}
}

