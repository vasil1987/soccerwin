using System;

namespace SoccerWin
{
	/// <summary>
	/// игра
	/// </summary>
	public class Game
	{
		public string Team1{ get; set; }
		public string Team2{ get; set; }
		public string Date{ get; set; }
		public dynamic Tag{ get; set; }

		public override bool Equals (object obj)
		{
			var other = obj as Game;
			return Team1 == other.Team1 && Team2 == other.Team2;
		}

		public override int GetHashCode ()
		{
			return (Team1 + Team2).GetHashCode ();
		}
	}
}

