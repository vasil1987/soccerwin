using System;
using System.Collections.Generic;

namespace SoccerWin.Tablesleague
{
	class TLGame{
		public string Team1{ get; set; }
		public string Team2{ get; set; }

		public FullTable Team1Table{ get; set; }
		public FullTable Team2Table{ get; set; }

		public IEnumerable<Step1> Team1H2H{ get; set; }
		public IEnumerable<Step1> Team2H2H{ get; set; }
	}
}

