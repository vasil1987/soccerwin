using System;

namespace SoccerWin.Tablesleague
{
	class Table
	{
		public int Team{ get; set; }

		/// <summary>
		/// И
		/// </summary>
		/// <value>The games.</value>
		public int Games{ get; set; }

		/// <summary>
		/// В
		/// </summary>
		/// <value>The wins.</value>
		public int Wins{ get; set; }

		/// <summary>
		/// Н
		/// </summary>
		/// <value>The draw.</value>
		public int Draw{ get; set; }

		/// <summary>
		/// П
		/// </summary>
		/// <value>The losts.</value>
		public int Losts{ get; set; }

		/// <summary>
		/// Г
		/// </summary>
		/// <value>The goals.</value>
		public int Goals1{ get; set; }

		public int Goals2{ get; set; }

		/// <summary>
		/// О
		/// </summary>
		/// <value>The points.</value>
		public int Points{ get; set; }
	}
}

