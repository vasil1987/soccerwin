using System;

namespace SoccerWin.Tablesleague
{
	class Step1
	{
		public string Date{ get; set; }

		public string Team1{ get; set; }

		public string Team2{ get; set; }

		public string Score{ get; set; }

		public int Goals1 {
			get {
				int res = 0;
				int.TryParse (Score.Split ('-') [0], out res);
				return res;
			}
		}

		public int Goals2 {
			get {
				int res = 0;
				int.TryParse (Score.Split ('-') [1], out res);
				return res;
			}
		}
	}
}

