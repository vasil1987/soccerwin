using System;

namespace SoccerWin.Tablesleague
{
	class FullTable
	{
		public Table Total{ get; set; }

		public Table Home{ get; set; }

		public Table Away{ get; set; }
	}
}

