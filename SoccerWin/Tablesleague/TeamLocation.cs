using System;

namespace SoccerWin.Tablesleague
{
	enum TeamLocation:int
	{
		All = 0,
		Home = 1,
		Away = 2
	}
}

