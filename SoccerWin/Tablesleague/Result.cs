using System;

namespace SoccerWin.Tablesleague
{
	
	class Result
	{
		public string Team1{ get; set; }

		public string Team2{ get; set; }

		public double C5{ get; set; }

		public double C6{ get; set; }

		public double D5{ get; set; }

		public double D6{ get; set; }

		public int I5 { get; set; }

		public int K5 { get; set; }

		public int E5{ get; set; }

		public int F5{ get; set; }

		public int G5{ get; set; }

		public int H5{ get; set; }

		public int E6{ get; set; }

		public int F6{ get; set; }

		public double G6{ get; set; }

		public double H6{ get; set; }

		public int I6 { get; set; }

		public int K6 { get; set; }

		public int I7 { get; set; }

		public int K7 { get; set; }

		public double D7{ get; set; }

		public double H7{ get; set; }

		public int I8 { get; set; }

		public int K8 { get; set; }

		public int I9 { get; set; }

		public int K9 { get; set; }

		public double I11 { get; set; }

		public double K11 { get; set; }

		public double I12 { get; set; }

		public double K12 { get; set; }

		public double I13 { get; set; }

		public double K13 { get; set; }

		public double D17{ get; set; }

		public double E17{ get; set; }

		public double F17{ get; set; }

		public double G17{ get; set; }

		public double H17{ get; set; }

		public double D18{ get; set; }

		public double E18{ get; set; }

		public double F18{ get; set; }

		public double G18{ get; set; }

		public double H18{ get; set; }

		public double E23{ get; set; }

		public double F23{ get; set; }

		public string G24{ get; set; }

		public string H24{ get; set; }

		public double C12 {get;set;}

		public double D12 {get;set;}
	}

}

