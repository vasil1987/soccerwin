using System;
using System.Collections.Generic;
using HtmlAgilityPack;
using System.Dynamic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Configuration;

namespace SoccerWin.Tablesleague
{
	class Method:IMethod
	{
		private DateTime lastLoadDate;
		private static Game[] gamesCache;

		public IEnumerable<Game> GetGames ()
		{
		var diff = DateTime.Now - lastLoadDate;
			if (diff.TotalHours < 3.0) {
				return gamesCache;
			}

			var doc = Load ("http://www.tablesleague.com/livescores/");


			var games = new HashSet<Game> ();

			var ids = doc.DocumentNode.SelectNodes ("//div[@data-league-id][count(*)=1]");

			var filledRows = doc.DocumentNode.SelectNodes ("//div[@data-status-type]");


			Action<HtmlNodeCollection> fill = (rws) => {
				foreach (var srow in rws) {

					dynamic tag = new ExpandoObject ();
					//tag.State = row.SelectSingleNode ("div[2]").Attributes ["title"].Value;
					tag.url = srow.SelectSingleNode ("div[6]/a").Attributes ["href"].Value;
					//tag.Score = row.SelectSingleNode (" div[4]/a/strong").InnerText;
					//tag.Tip1 = row.SelectSingleNode (" div[7]/div[1]").InnerText;
					//tag.TipX = row.SelectSingleNode (" div[7]/div[2]").InnerText;
					//tag.Tip2 = row.SelectSingleNode (" div[7]/div[3]").InnerText;

					var game = new Game {
						Team1 = srow.SelectSingleNode ("div[4]/a/span[1]").InnerText,
						Team2 = srow.SelectSingleNode ("div[4]/a/span[2]").InnerText,
						Date = srow.SelectSingleNode ("div[2]/span[2]").InnerText,
						Tag = tag
					};
					if (!games.Contains (game)) {
						games.Add (game);
					}
				}
			};

			fill (filledRows);

			foreach (var id in ids) {
				try{
				var ham = id.Attributes ["data-league-id"].Value;

				var sdoc = Load (string.Format ("http://www.tablesleague.com/xml/h2h_league_games_list/?lang=2&league_id={0}&date={1:yyyy-MM-dd}", ham, DateTime.Today));

				var srows = sdoc.DocumentNode.SelectNodes ("div[@data-status-type]");
				fill (srows);
				}catch(Exception ex) {

				}
				///<div class="form won next_game_h2h" onclick="window.location='/?game_id=1445688'" data-time="1449932400" data-info="Montrose <b>1-3</b> Stirling A.">W</div>
			}

			gamesCache = games.ToArray();
			lastLoadDate = DateTime.Now;
			return games.ToArray();
		}

		private double poisson(double lam,int k)
		{
			try{
				return MathNet.Numerics.Distributions.Poisson.PMF(lam,k);
			}catch(Exception ex){

			}
			return 0.0;
		}

		public dynamic Calc (Game game1)
		{
			try {
				var game = new TLGame ();

				var url = "http://www.tablesleague.com/" + game1.Tag.url;
				var doc = Load (url);
				var id1 = doc.DocumentNode.SelectSingleNode ("//div[@class='team_a' and @data-team_id]").Attributes ["data-team_id"].Value;
				var id2 = doc.DocumentNode.SelectSingleNode ("//div[@class='team_b' and @data-team_id]").Attributes ["data-team_id"].Value;


				game.Team1 = doc.DocumentNode.SelectSingleNode ("//div[@class='team_a' and @data-team_id]/div/text()[2]").InnerText.Trim ();
				game.Team2 = doc.DocumentNode.SelectSingleNode ("//div[@class='team_b' and @data-team_id]/div/text()[2]").InnerText.Trim ();


				var url1 = doc.DocumentNode.SelectSingleNode ("//div[@class='note_team_a']/a[1]").Attributes ["href"].Value;
				var url2 = doc.DocumentNode.SelectSingleNode ("//div[@class='note_team_b']/a[1]").Attributes ["href"].Value;
				game.Team1Table = LoadTable (url1);
				game.Team2Table = LoadTable (url2);

				game.Team1H2H = LoadH2H (id1, TeamLocation.All);
				game.Team2H2H = LoadH2H (id2, TeamLocation.All);

				Result result = new Result ();

				result.Team1 = game.Team1;
				result.Team2 = game.Team2;


				result.I5 = game.Team1Table.Total.Games;
				result.K5 = game.Team2Table.Total.Games;

				result.I6 = game.Team1Table.Home.Goals1;
				result.K6 = game.Team2Table.Home.Goals1;

				result.I7 = game.Team1Table.Away.Games;
				result.K7 = game.Team2Table.Away.Games;

				result.I8 = game.Team1Table.Home.Goals2;
				result.K8 = game.Team2Table.Home.Goals2;
				result.I9 = game.Team1Table.Away.Goals2;
				result.K9 = game.Team2Table.Away.Goals2;

				////лист диагноз3 (2)
				//Голов забито дома (b3)
				double b3 = game.Team1Table.Home.Goals1 + game.Team2Table.Home.Goals1;

				//Голов пропущено дома (b4) 
				//Голов забито в гостях (b5)
				double b5 = game.Team1Table.Away.Goals1 + game.Team2Table.Away.Goals1;

				//Голов пропущено в гостях (b6)
				//Кол-во команд (b7)
				double b7 = 2;

				//Игр сыграно дома (b8)
				double b8 = (game.Team1Table.Home.Games + game.Team2Table.Home.Games) / 2.0;

				//Игр сыграно в гостях (b9)
				double b9 = (game.Team1Table.Away.Games + game.Team2Table.Away.Games) / 2;

				//Ср. забитых дома (b11)
				double b11 = b3 / b7 / b8;

				//Ср. забитых в гостях (b12)
				double b12 = b5 / b7 / b9;

				//хозяева
				//Забито дома (e3)
				double e3 = game.Team1Table.Home.Goals1;
				//Пропущено дома (e4)
				double e4 = game.Team1Table.Home.Goals2;
				//Матчей дома (e5)
				double e5 = game.Team1Table.Home.Games;
				//Сила атаки (e6)
				double e6 = e3 / e5 / b11;
				//Сила обороны (e7)
				double e7 = e4 / e5 / b11;


				//Гости	
				//Забито в гостях (i3)
				double i3 = game.Team2Table.Away.Goals1;
				//Пропущено в гостях (i4)
				double i4 = game.Team2Table.Away.Goals2;
				//Матчей в гостях (i5)
				double i5 = game.Team2Table.Away.Games;
				//Сила атаки (i6)
				double i6 = i3 / i5 / b12;
				//Сила обороны (i7)
				double i7 = i4 / i5 / b11;
				//Расчет голов (i8)
				double i8 = i6 * e7 * b12;

				//Хозяева
				//Расчет голов (e8)
				double e8 = e6 * i7 / b11;

				result.C6 = poisson (e8, 0) * 100.0;

				result.C5 = Enumerable.Range (0, 6).Select (cell => new {
					i = cell,
					p = poisson (e8, cell) * 100.0						
				}).Where (i => i.p > 21.0).Sum (i => i.i);

				result.D6 = poisson (i8, 0) * 100.0;

				result.D5 = Enumerable.Range (0, 6).Select (cell => new {					
					i = cell,
					p = poisson (i8, cell) * 100.0
				}).Where (i => i.p > 21.0).Sum (i => i.i);

				result.G5 = Enumerable.Range (0, 6).Select (cell => new {
				i = cell,
				p = poisson (e8, cell) * 100.0
			}).Where (i => i.p > 20.0).Sum (i => i.i);

				result.H5 = Enumerable.Range (0, 6).Select (cell => new {					
				i = cell,
				p = poisson (i8, cell) * 100.0
			}).Where (i => i.p > 20.0).Sum (i => i.i);

				result.I11 = e3 / e5 / b11;
				result.K11 = i3 / i5 / b12;

				result.I12 = e4 / e5 / b11;
				result.K12 = i4 / i5 / b11;
				result.I13 = 1;
				result.K13 = 1;

				//---


				////лист диагноз3 (2)
				//Голов забито дома (b3)
				double d3_b3 = game.Team1H2H.Sum (t => t.Goals1) + game.Team2H2H.Sum (t => t.Goals1);

				//Голов пропущено дома (b4) 
				//Голов забито в гостях (b5)
				double d3_b5 = game.Team1H2H.Sum (t => t.Goals2) +
					game.Team2H2H.Sum (t => t.Goals2);

				//Голов пропущено в гостях (b6)
				//Кол-во команд (b7)
				double d3_b7 = 2;

				//Игр сыграно дома (b8)
				double d3_b8 = (game.Team1H2H.Count () + game.Team2H2H.Count ()) / 2.0;

				//Игр сыграно в гостях (b9)
				double d3_b9 = (game.Team1H2H.Count () +
					game.Team2H2H.Count ()) / 2;

				//Ср. забитых дома (b11)
				double d3_b11 = d3_b3 / d3_b7 / d3_b8;

				//Ср. забитых в гостях (b12)
				double d3_b12 = d3_b5 / d3_b7 / d3_b9;

				//хозяева
				//Забито дома (e3)
				double d3_e3 = game.Team1H2H.Where (t => t.Team1 == result.Team1).Sum (t => t.Goals1);
				//Пропущено дома (e4)
				double d3_e4 = game.Team1H2H.Where (t => t.Team1 == result.Team1).Sum (t => t.Goals2);
				//Матчей дома (e5)
				double d3_e5 = game.Team1H2H.Where (t => t.Team1 == result.Team1).Count ();
				//Сила атаки (e6)
				double d3_e6 = d3_e3 / d3_e5 / d3_b11;
				//Сила обороны (e7)
				double d3_e7 = d3_e4 / d3_e5 / d3_b11;


				double d3_i4 = game.Team2H2H.Sum (t => t.Goals1);
				double d3_i5 = game.Team2H2H.Count ();


				double d3_i7 = d3_i4 / d3_i5 / d3_b11;

				double d3_e8 = d3_e6 * d3_i7 / d3_b11;
				result.G6 = poisson (d3_e8, 0) * 100.0;

				result.G5 = Enumerable.Range (0, 6).Select (cell => new {
				i = cell,
				p = poisson (d3_e8, cell) * 100.0
			}).Where (i => i.p > 20.0).Sum (i => i.i);

				double d3_i3 = game.Team2H2H.Sum (t => t.Goals2);
				double d3_i6 = d3_i3 / d3_i5 / d3_b12;
				double d3_i8 = d3_i6 * d3_e7 * d3_b12;
				result.H6 = poisson (d3_i8, 0) * 100.0;

				result.H5 = Enumerable.Range (0, 6).Select (cell => new {					
				i = cell,
				p = poisson (d3_i8, cell) * 100.0
			}).Where (i => i.p > 20.0).Sum (i => i.i);

				//--


				double g32_b10 = game.Team1H2H.Where (t => t.Team1 == result.Team1).Sum (t => t.Goals1);
				double g32_e11 = game.Team2H2H.Where (t => t.Team2 == result.Team2).Sum (t => t.Goals1);
				double g32_b12 = game.Team1H2H.Take (10).Where (t => t.Team1 == result.Team1).Count ();
				double g32_e12 = game.Team1H2H.Take (10).Where (t => t.Team2 == result.Team1).Count ();
				double g32_c2 = g32_b10 / g32_b12;
				double g32_c3 = g32_e11 / g32_e12;
				double g32_c4 = (g32_c2 + g32_c3) / 2.0;

				double g32_b11 = game.Team1H2H.Where (t => t.Team1 == result.Team1).Sum (t => t.Goals2);
				double g32_f3 = g32_b11 / g32_b12;
				double g32_e10 = game.Team2H2H.Where (t => t.Team2 == result.Team2).Sum (t => t.Goals2);
				double g32_f2 = g32_e10 / g32_e12;
				double g32_f4 = (g32_f2 + g32_f3) / 2.0;

				result.E5 = (int)Math.Round (g32_c4, 0);
				result.F5 = (int)Math.Round (g32_f4, 0);


				double a2_o9 = (game.Team1H2H.Sum (t => t.Goals1 + t.Goals2) + game.Team1H2H.Sum (t => t.Goals1 + t.Goals2)) / (game.Team1H2H.Count () + game.Team2H2H.Count ());
				double a2_b4 = game.Team2H2H.Count ();
				double a2_g4 = game.Team2H2H.Where (t => t.Team1 == result.Team2).Sum (t => t.Goals2) + game.Team2H2H.Where (t => t.Team2 == result.Team2).Sum (t => t.Goals1);
				double a2_e7 = a2_g4 / a2_b4;
				double a2_p4 = game.Team2H2H.Where (t => t.Team2 == result.Team2).Count ();
				double a2_u4 = game.Team2H2H.Where (t => t.Team2 == result.Team2).Sum (t => t.Goals1);
				double a2_r7 = a2_u4 / a2_p4;
				double a2_b3 = game.Team1H2H.Count ();
				double a2_f3 = game.Team1H2H.Where (t => t.Team1 == result.Team1).Sum (t => t.Goals1) + game.Team1H2H.Where (t => t.Team2 == result.Team1).Sum (t => t.Goals2);
				double a2_b6 = a2_f3 / a2_b3;
				double a2_i3 = game.Team1H2H.Where (t => t.Team1 == result.Team1).Count ();
				double a2_m3 = game.Team1H2H.Where (t => t.Team1 == result.Team1).Sum (t => t.Goals1);
				double a2_i6 = a2_m3 / a2_i3;
				double a2_f21 = (a2_i6 + a2_b6) / 2.0 + (a2_r7 + a2_e7) / 2.0 - (1.25 + a2_o9) / 2.0;
				double a2_n16 = (a2_i6 + a2_b6) / 2.0;
				result.E6 = (int)Math.Round ((a2_f21 + a2_n16) / 2.0, 0);

				double a2_g3 = game.Team1H2H.Where (t => t.Team1 == result.Team1).Sum (t => t.Goals2) + game.Team1H2H.Where (t => t.Team2 == result.Team1).Sum (t => t.Goals1);
				double a2_e6 = a2_g3 / a2_b3;
				double a2_n3 = game.Team1H2H.Where (t => t.Team1 == result.Team1).Sum (t => t.Goals2);
				double a2_l6 = a2_n3 / a2_i3;
				double a2_f4 = game.Team2H2H.Where (t => t.Team1 == result.Team2).Sum (t => t.Goals1) + game.Team2H2H.Where (t => t.Team2 == result.Team2).Sum (t => t.Goals2);
				double a2_b7 = a2_f4 / a2_b4;
				double a2_t4 = game.Team2H2H.Where (t => t.Team2 == result.Team2).Sum (t => t.Goals2);
				double a2_o7 = a2_t4 / a2_p4;
				double a2_g21 = (a2_o7 + a2_b7) / 2.0 + (a2_l6 + a2_e6) / 2.0 - (1.25 + a2_o9) / 2.0;
				double a2_o16 = (a2_o7 + a2_b7) / 2.0;

				result.F6 = (int)Math.Round ((a2_g21 + a2_o16) / 2.0, 0);

				double m1_a5 = a2_n16;
				double m1_b5 = a2_o16;

				var hg = new double[8];
				var ag = new double[8];
				for (var i=0; i<=7; i++) {
					hg [i] = poisson (m1_a5, i);
					ag [i] = poisson (m1_b5, i);
				}

				var expHW = new List<HW> ();

				for (var i=7; i>0; i--) {
					for (var j=0; j<i; j++) {
						expHW.Add (new HW { A=i, B=j, EXP = (hg[i]*ag[j])/100 });
					}
				}

				var expAW = new List<HW> ();

				for (var i=7; i>0; i--) {
					for (var j=0; j<i; j++) {
						expAW.Add (new HW { A=j, B=i, EXP = (hg[j]*ag[i])/100 });
					}
				}

				var expDR = new List<HW> ();

				for (var i=0; i<7; i++) {
					expDR.Add (new HW { A=i, B=i, EXP = (hg[i]*ag[i])/100 });
				}


				//обе забьют
				result.D7 = expAW.Union(expDR).Union(expHW).Where(s=>s.A!=0 && s.B!=0).Sum(s=>s.EXP)*100.0;//
				//одна не забьет
				result.H7 = expAW.Union(expDR).Union(expHW).Where(s=>(s.A==0 && s.B!=0)||(s.A!=0 && s.B==0)).Sum(s=>s.EXP)*100.0;//

				double[][] p1_f19xh26 = new double[3][];
				p1_f19xh26 [0] = new double[8];
				p1_f19xh26 [1] = new double[8];
				p1_f19xh26 [2] = new double[8];
				for (int i = 0; i <= 7; i++) {
					p1_f19xh26 [0] [i] = i;
					p1_f19xh26 [1] [i] = poisson (m1_a5, i);
					p1_f19xh26 [2] [i] = poisson (m1_b5, i);
				}

				//777777766666655555...
				int[] p1_j19 = new int[28];
				var k = 0;
				for (var i = 7; i > 0; i--) {
					for (var j = 0; j < i; j++) {
						p1_j19 [k++] = i;
					}
				}

				//01234560123450123...
				int[] p1_k19 = new int[28];
				k = 0;
				for (var i = 7; i > 0; i--) {
					for (var j = 0; j < i; j++) {
						p1_k19 [k++] = j;
					}
				}

				result.D17 = Enumerable.Range (0, 27).Sum (i => {
					double p1_o19 = p1_f19xh26 [1] [p1_j19 [i]];//??0.0;
					double p1_p19 = p1_f19xh26 [2] [p1_k19 [i]];//??0.0;
					return p1_o19 * p1_p19;
				}) * 100.0;


				result.E17 = Enumerable.Range (0, 6).Sum (i => {
					double p1_o81 = p1_f19xh26 [1] [i];//??0.0;
					double p1_p81 = p1_f19xh26 [2] [i];//??0.0;
					return p1_o81 * p1_p81;
				}) * 100.0;

				result.F17 = Enumerable.Range (0, 27).Sum (i => {
					double p1_o50 = p1_f19xh26 [1] [p1_k19 [i]];//??0.0;
					double p1_p50 = p1_f19xh26 [2] [p1_j19 [i]];//??0.0;
					return p1_o50 * p1_p50;
				}) * 100.0;

				result.G17 = p1_f19xh26 [1] [2] * p1_f19xh26 [2] [0] +
					p1_f19xh26 [1] [1] * p1_f19xh26 [2] [0] +
					p1_f19xh26 [1] [0] * p1_f19xh26 [2] [2] +
					p1_f19xh26 [1] [0] * p1_f19xh26 [2] [1] +
					p1_f19xh26 [1] [0] * p1_f19xh26 [2] [0] +
					p1_f19xh26 [1] [1] * p1_f19xh26 [2] [1] * 100.0;

				result.H17 = 100.0 - result.G17;

				result.D18 = 100.0 / result.D17;
				result.E18 = 100.0 / result.E17;
				result.F18 = 100.0 / result.F17;
				result.G18 = 100.0 / result.G17;
				result.H18 = 100.0 / result.H17;

				result.E23 = e6 * e7 / b11;
				result.F23 = i6 * e7 * b12;

				result.G24 = a2_n16 > 1 ? "Yes" : "No";
				result.H24 = a2_o16 > 1 ? "Yes" : "No";

				var t2_l2=(a2_i6+a2_b6)/2.0+(a2_r7+a2_e7)/2.0-(1.25+a2_o9)/2.0;
				var t2_m2=(a2_o7+a2_b7)/2.0+(a2_l6+a2_e6)/2.0-(1.25+a2_o9)/2.0;

				var t2_k2=Math.Round(t2_l2+t2_m2,MidpointRounding.AwayFromZero);

				var t2_v2=a2_n16;
				var t2_w2=a2_o16;

				var t2_y2=(t2_l2+t2_v2)/2.0;
				var t2_z2=(t2_m2+t2_w2)/2.0;

				var cntIfTB=0+((t2_l2+t2_m2)>2.5?1:0)+((t2_k2>2.5)?1:0)+
					((t2_v2+t2_w2)>2.5?1:0)+
						((t2_y2+t2_z2)>2.5?1:0);

				var cntIfTM=0+((t2_l2+t2_m2)>2.5?0:1)+((t2_k2>2.5)?0:1)+
					((t2_v2+t2_w2)>2.5?0:1)+
						((t2_y2+t2_z2)>2.5?0:1);

				result.C12=cntIfTB/5.0*100.0;
				result.D12=cntIfTM/5.0*100.0;
				return result;
			} catch (Exception ex) {
				var foo = ex;
			}
			return new Result { Team1 = "Недостаточно данных" };
		}

		class HW
		{
			public int A{ get; set; }

			public int B{ get; set; }

			public double EXP{ get; set; }
		}

		public string Id {
			get {
				return "tablesleague";
			}
		}

		public string Name {
			get {
				return "Goal+";
			}
		}

		public string Template {
			get {
				return "tablesleague.html";
			}
		}

		private IEnumerable<Step1> LoadH2H (string teamId, TeamLocation location)
		{			
			var fd = Load ("http://www.tablesleague.com/xml/team_games_list/?team_id=" + teamId + "&farmer=" + (int)location + "&items=28&lang=2");

			var rows = fd.DocumentNode.SelectNodes ("//tr");

			var teamSeparator = new Regex (@"(<strong>)?(?<team1>[A-Za-z0-9\s]+)(</strong>)?\s-\s(<strong>)?(?<team2>[A-Za-z0-9\s]+)(</strong>)?");

			var steps1 = new List<Step1> ();

			foreach (var row in rows) {
				//Norwich - <strong>Liverpool</strong>
				var step1 = new Step1 ();

				var foo = row.SelectSingleNode ("td[3]/a").InnerText;
				foo = foo.Replace ("<strong>", "").Replace ("</strong>", "");
				var teams = foo.Replace (" - ", "|").Split ('|');
				step1.Team1 = teams [0];
				step1.Team2 = teams [1];


				step1.Date = row.SelectSingleNode ("td[2]").InnerText;
				step1.Score = row.SelectSingleNode ("td[4]").InnerText;
				steps1.Add (step1);
			}

			return steps1;
		}

		private FullTable LoadTable (string url)
		{			
			var doc = Load ("http://www.tablesleague.com" + url);

			var tab = doc.DocumentNode.SelectSingleNode ("//div[@class='content']/table[1]");

			Func<string, int> ip = (str) => {
				int res = 0;
				int.TryParse (str, out res);
				return res;
			};

			return new FullTable {

				Total = new Table {
					Games =ip( tab.SelectSingleNode ("tr[5]/td[2]").InnerText),
					          Wins = ip(tab.SelectSingleNode ("tr[6]/td[2]").InnerText),
					          Draw = ip(tab.SelectSingleNode ("tr[7]/td[2]").InnerText),
					          Losts = ip(tab.SelectSingleNode ("tr[8]/td[2]").InnerText),
					           Goals1 = ip(tab.SelectSingleNode ("tr[9]/td[2]").InnerText),
					            Goals2 = ip(tab.SelectSingleNode ("tr[10]/td[2]").InnerText)
				},
				Home = new Table {
						Games = ip(tab.SelectSingleNode ("tr[5]/td[4]").InnerText),
						           Wins = ip(tab.SelectSingleNode ("tr[6]/td[4]").InnerText),
						          Draw = ip(tab.SelectSingleNode ("tr[7]/td[4]").InnerText),
						          Losts = ip(tab.SelectSingleNode ("tr[8]/td[4]").InnerText),
						           Goals1 = ip(tab.SelectSingleNode ("tr[9]/td[4]").InnerText),
						            Goals2 = ip(tab.SelectSingleNode ("tr[10]/td[4]").InnerText)
				},
				Away = new Table {
							Games = ip(tab.SelectSingleNode ("tr[5]/td[6]").InnerText),
							           Wins = ip(tab.SelectSingleNode ("tr[6]/td[6]").InnerText),
							          Draw = ip(tab.SelectSingleNode ("tr[7]/td[6]").InnerText),
							          Losts = ip(tab.SelectSingleNode ("tr[8]/td[6]").InnerText),
							           Goals1 = ip(tab.SelectSingleNode ("tr[9]/td[6]").InnerText),
							            Goals2 = ip(tab.SelectSingleNode ("tr[10]/td[6]").InnerText)
				}
			};
		}

		private HtmlDocument Load (string url)
		{
			var web = new HtmlWeb ();
			web.UseCookies = true;
			web.PreRequest += (req) => {
				req.CookieContainer = new System.Net.CookieContainer ();
				req.CookieContainer.Add (new System.Net.Cookie ("auto_timezone", "Asia/Yekaterinburg", "/", "req.CookieContainer.Add"));
				req.CookieContainer.Add (new System.Net.Cookie ("timezone_name", "Etc/GMT-3", "/", "req.CookieContainer.Add"));
				req.CookieContainer.Add (new System.Net.Cookie ("timezone_set_offset", "-3", "/", "req.CookieContainer.Add"));
				return true;
			};

			if (bool.Parse (ConfigurationManager.AppSettings ["use-proxy"])) {
				return web.Load (url, ConfigurationManager.AppSettings ["proxy-url"], 
				                 int.Parse (ConfigurationManager.AppSettings ["proxy-port"]), 
				                 ConfigurationManager.AppSettings ["proxy-login"], ConfigurationManager.AppSettings ["proxy-pass"]);
			}
			return web.Load (url);
		}
	}
}

