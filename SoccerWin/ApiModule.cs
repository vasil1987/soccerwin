﻿using System;
using System.Linq;
using Nancy;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;
using Newtonsoft.Json;

namespace SoccerWin
{
	public class ApiModule:NancyModule
	{
		const int LIMIT = 100;

		public ApiModule (MethodManager mm)
		{
			Get ["/methods"] = (_) => {
				return mm.Methods;
			};

			Get ["/games/{methodId}/{skip}"] = (parameters) => {				
				try {

					int skip=parameters.skip;

					var method = mm.Methods.FirstOrDefault (m => m.Id == parameters.methodId);
					if (method == null) {
						return "метод не найден";
					}

					var haa = method.GetGames ();
					
					return Response.AsJson(haa.Skip(skip).Take(LIMIT));
				} catch (Exception ex) {
					var foo = ex;
				}
				return null;
			};

			Post ["/calc/{methodId}"] = (parameters) => {
				try {
					var pid = (string)parameters.methodId;
					var method = mm.Methods.FirstOrDefault (m => m.Id == parameters.methodId);
					if (method == null) {
						return "метод не найден";
					}

					var len = (int)this.Request.Body.Length;
					var buffer = new byte[len];
					this.Request.Body.Read (buffer, 0, len);
					var json = Encoding.UTF8.GetString (buffer);
					var game = JsonConvert.DeserializeObject<Game> (json);

					return method.Calc (game);
				} catch (Exception ex) {
					var foo = ex;
				}
				return "";
			};
		}
	}
}

