﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Http;
using Microsoft.Owin.Hosting;
using System.Data.OleDb;
using Topshelf;

namespace SoccerWin
{
	class MainClass
	{
		public static void Main (string[] args)
		{		
			HostFactory.Run (h =>
			{
				h.UseLinuxIfAvailable ();
				h.Service<Service> (s =>
				{
					s.ConstructUsing (name => new Service ());
					s.WhenStarted (os => os.Start ());
					s.WhenStopped (os => os.Stop ());
				});

				h.RunAsLocalSystem ();
				h.SetDescription ("SoccerWin");
				h.SetDisplayName ("SoccerWin");
				h.SetServiceName ("SoccerWin");
			});
		}
	}
}
