﻿using System;
using Nancy;
using System.IO;
using Nancy.Responses;

namespace SoccerWin
{
	public class IndexModule:NancyModule
	{
		public IndexModule ()
		{
			Get[@"/"] = parameters =>
			{
				return File.ReadAllText("Content/index.html");
			};		
		}
	}
}

