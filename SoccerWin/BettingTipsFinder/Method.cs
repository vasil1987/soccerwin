using System;
using System.Collections.Generic;
using HtmlAgilityPack;
using System.Linq;
using System.Dynamic;
using System.Configuration;

namespace SoccerWin.BettingTipsFinder
{
	class Method:IMethod
	{
		public IEnumerable<Game> GetGames ()
		{
			var doc = Load ("http://www.betting-tips-finder.com/");
			var rows = doc.DocumentNode.SelectNodes ("//body/table/tr/td[2]/center[2]/table/tr[position() mod 2 = 0 and position() > 1]");			 
			var games = new List<Game> ();
			foreach (var row in rows) {
				//row.SelectSingleNode ("td[1]").InnerText;
				dynamic tag = new ExpandoObject ();
				tag.tip = row.SelectSingleNode ("td[3]").InnerText.Replace ("&nbsp;", "").Trim();
				tag.odd = row.SelectSingleNode ("td[4]").InnerText.Replace ("&nbsp;", "");
				tag.match = row.SelectSingleNode ("td[6]").InnerText.Replace ("&nbsp;", "").Replace('�','$');
				var teams = row.SelectSingleNode ("th[1]").InnerText.Split('-');

				var game = new Game {
					Team1 = teams[0].Trim(),
					Team2 = teams[1].Trim(),
					Date = row.SelectSingleNode ("td[1]").InnerText+" "+row.SelectSingleNode ("td[2]").InnerText,
					Tag = tag
				};
				games.Add (game);
			}
			return games;
		}

		public dynamic Calc (Game game)
		{
			string tip = game.Tag.tip;
			string odd = game.Tag.odd;
			string match = game.Tag.match;
            
			dynamic res = new ExpandoObject ();

			res.tip = tip;
			res.odd = odd;
			res.match = match;
			res.team1 = game.Team1;
			res.team2 = game.Team2;
			return res;
		}

		private HtmlDocument Load (string url)
		{
			var web = new HtmlWeb ();

			if (bool.Parse (ConfigurationManager.AppSettings ["use-proxy"])) {
				return web.Load (url, ConfigurationManager.AppSettings ["proxy-url"], 
				                int.Parse (ConfigurationManager.AppSettings ["proxy-port"]), 
				                ConfigurationManager.AppSettings ["proxy-login"], ConfigurationManager.AppSettings ["proxy-pass"]);
			}
			return web.Load (url);
		}

		public string Id {
			get {
				return "bettingtipsfinder";
			}
		}

		public string Name {
			get {
				return "Betfair market";
			}
		}

		public string Template {
			get {
				return "bettingtipsfinder.html";
			}
		}
	}
}

