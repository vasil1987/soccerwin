using System;
using Nancy.Hosting.Self;
using Microsoft.Practices.Unity;

namespace SoccerWin
{
	public class Service
	{
		private NancyHost nancyHost;

		public void Start ()
		{
			var uc = new UnityContainer ();

			var mm = new MethodManager ();
			mm.AddMethod(new BettingTipsFinder.Method());
			mm.AddMethod(new StatFootball.Method());
			mm.AddMethod (new Tablesleague.Method ());

			uc.RegisterInstance (mm);

			var url = new Uri ("http://localhost:8080");
			nancyHost = new Nancy.Hosting.Self.NancyHost (new Bootstrapper (uc), url);
			nancyHost.Start ();
		}

		public void Stop ()
		{
			nancyHost.Stop ();
		}
	}
}

