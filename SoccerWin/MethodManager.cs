using System;
using System.Collections.Generic;

namespace SoccerWin
{
	public class MethodManager
	{
		private List<IMethod> methods = new List<IMethod> ();

		public IEnumerable<IMethod> Methods {
			get {
				return methods;
			}
		}

		public void AddMethod (IMethod method)
		{
			methods.Add (method);
		}
	}
}

